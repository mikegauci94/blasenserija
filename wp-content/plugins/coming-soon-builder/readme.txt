=== Coming Soon ===
Contributors: umarbajwa
Donate link: http://web-settler.com/coming-soon/
Tags: coming soon,coming soon page, coming soon mode, maintenance mode, maintenance page, coming soon landing page, site launch,mailchimp, countdown timer,site offline, launch page, under construction, WordPress coming soon, WordPress maintenance mode,mailchimp coming soon, comingsoon, coming soon website, coming soon themes, coming soon theme, landing page, landing page templates
Requires at least: 3.4
Tested up to: 4.5
Stable tag: 1.4
License: GPLv2 or later

Create stunning Coming Soon and Maintenance Mode pages with WordPress coming soon plugin.

== Description ==

With WordPress coming soon plugin you can create coming soon pages and maintenance mode pages and restrict your public visitors from visiting your under construction or non-launched website.

WordPress coming soon plugin is very easy and simple to use. Coming soon plugin's option panel is designed to maximize results in minimum time. You can set your coming soon landing page or maintenance mode page in less than 2 minutes.

### All you need

WordPress coming soon plugin have all the options and settings a user can use to design his own coming soon page.

### Customizations

Coming Soon plugin allows you to design your own coming soon page with color customizations.

### Google Fonts

Integrated Google fonts make your coming soon page stand out and give stunning look to your coming soon page.

### MailChimp Forms

Coming soon page has integrated MailChimp Subscribe form to connect with you. When your website is ready notify them about it and get amazing results.

### Pre Designed Themes

WordPress coming soon plugin have five beautiful pre designed themes.

### Custom CSS & JS

WordPress coming soon plugin supports custom CSS and JavaScript so you can enhance the functionality and design of your coming soon landing page.


**WordPress coming soon plugin have following **

### Features : 

* 	**Responsive coming soon page.**
* 	**Responsive five coming soon themes.**
* 	**Responsive maintenance mode page.**
* 	**Progress bar for maintenance mode & coming soon page.**
* 	**Hundreds of Google Fonts for coming soon.**
* 	**Set favicon for coming soon page.**
* 	**Set Logo for coming soon page.**
* 	**Background color customizations for coming soon page.**
* 	**Text color customization for coming soon page.**
* 	**Background Image for coming soon page.**
* 	**SEO friendly coming soon page.**
* 	**SEO friendly maintenance mode page.**
* 	**Supports analytics for coming soon page.**
* 	**Social Facebook icon for coming soon page.**
* 	**Social Twitter icon for coming soon page.**
* 	**Social Pinterest icon for coming soon page.**
* 	**Social LinkedIn icon for coming soon page.**
* 	**Social Instagram icon for coming soon page.**
* 	**Embed Videos in coming soon page.**
* 	**Select text font family of coming soon page.**
* 	**Select heading font family of coming soon page.**
* 	**Shortcode Support.**

###  <a href="http://web-settler.com/coming-soon/"> Premium Features : </a>

* 	**Six Coming Soon themes.**
* 	**Add Custom CSS in coming soon pages.**
* 	**Add custom JavaScript in coming soon page.**
* 	**Countdown timer for coming soon page.**
* 	**WYSIWYG editor.**
* 	**MailChimp Integration.**
* 	**Google fonts integration.**


### The most simple coming soon builder plugin just add your content enable coming soon mode and your coming soon page is ready for all public visitors.

WordPress Coming Soon plugin with 6 modern themes and Countdowns is the most all in one desired and use Plugin on his niche. WordPress Coming Soon plugin let you continue your work on website while your visitors see the Coming Soon page.

Coming Soon plugin have pre designed themes for you to easily setup your Coming soon page in minutes which is rich in features and also looks stunning.

The WordPress Coming soon plugin works without any problem with all popular WordPress themes, so you can use WordPress coming soon plugin on any theme you like without any worries. If something goes wrong or does not work just let us know about it.

After enabling coming soon or maintenance mode feature logged out users will see coming soon page while logged in user will be able to access the website.

Coming soon plugin is ideal for web developers who want to show their clients that website is in Coming soon or maintenance mode while working on the website.

You are developing your website, but you also want to make it visible for your audience. You can't show your visitors broken websites in this time you need a good looking coming soon page.

Don't want to show your potential visitors broken web pages or website use coming soon plugin. Coming soon is very easy to setup and a must have tool for all websites, Coming soon plugin can also act as acquaintance mode.

A coming soon page is not only a landing page that hints visitors about the up-coming website or web services, it is also used by designers and web masters to increase the level of anticipations as well as collecting data of interested users by inserting a “notify me when launch” subscription button.

Your coming soon page should give your visitor proper information about your website, service  or product. Few of important points are listed below : 

*  **Your coming soon page should explains the site.**
*  **Your coming soon page should encourage subscription.**
*  **Your coming soon page should keep visitors updated via social networks.**
*  **Your coming soon page should provide useful contacts.**
*  **Your coming soon page should create curiosity.**


== Installation ==

= The easy way : =

1. Go to the Plugins Menu in WordPress.
1. Search for plugin "Coming Soon Builder".
1. Click "Install".
1. After Installation click activate to start using the coming soon plugin.

* Go to Coming soon from Dashboard menu.
* Enable Coming Soon feature.


= Not so easy way : =

1. To install Coming Soon via FTP
1. Download the Coming Soon plugin
1. Unarchive Coming Soon plugin
1. Copy folder with coming-soon-builder.zip
1. Open the ftp \wp-content\plugins\
1. Paste the plug-ins folder in the folder
1. Go to admin panel => open item "Plugins" => activate Coming Soon builder

* Go to Coming soon from Dashboard menu.
* Enable Coming Soon feature.

== Frequently Asked Questions ==

= Q. What is a coming soon page ? =

A: A coming soon page allows you to work on your website and in meantime you can show your visitors a page where they can get information about when your website is going to get live or completed. Coming soon plugin also supports subscribe form so you can notify your subscribers.

= Q: Can I create maintenance mode page with WordPress Coming Soon plugin ? =

A : Yes, WordPress coming soon plugin also supports maintenance mode feature.

= Q: Can I upload Logo Image for my coming soon page ? =

A : Yes, You can upload logo and background images for your coming soon page.

= Q: Can I access my website when coming soon mode is activated ? =

A: Yes, Coming soon plugin allows you to view your website.    

= Q: Can I add a progress bar to my coming soon page which shows the progress of my website ? =

A: Yes, There are coming soon themes which support progress bar.

= Q: How many pre designed themes are in coming soon plugin ? =

A: There are six pre designed coming soon themes.

= Q: Are these comign soon themes responsive ? = 

A: Yes, All these coming soon themes are responsive & Mobile friendly.

= Q: Who would be able to visit the website when coming soon mode is active ? =

A: Only the logged in user will be able to visit the website when coming soon mode is activated.

= Q: What content my coming soon page should have ? = 

A: Your coming soon page should give your visitor proper information about your website, service  or product. Few of important points are listed below : 

* Your coming soon page should explains the site.
* Your coming soon page should encourage subscription.
* Your coming soon page should keep visitors updated via social networks.
* Your coming soon page should provide useful contacts.
* Your coming soon page should create curiosity.


== Other Notes ==

= What is a Coming Soon Page ? =

A coming soon page allows you to work on your website and in meantime you can show your visitors a page where they can get information about when your website is going to get live or completed. Coming soon plugin also supports subscribe form so you can notify your subscribers.


You are developing your website, but you also want to make it visible for your audience. You can't show your visitors broken websites in this time you need a good looking coming soon page. A coming soon page or Launch page is a combination of 3 important features .

1. A Subscribe form in your coming soon page that allows you to collect emails.
1. A countdown timer in your coming soon page to tell your visitors when the website will be live.
1. Social Media network buttons to get tons of followers to promote your upcoming products.


Your coming soon page should give your visitor proper information about your website, service  or product. Few of important points are listed below : 

* Your coming soon page should explains the site.
* Your coming soon page should encourage subscription.
* Your coming soon page should keep visitors updated via social networks.
* Your coming soon page should provide useful contacts.
* Your coming soon page should create curiosity.


== Screenshots ==

1. Coming soon theme.

2. Coming soon theme.

3. Coming soon theme.

4. Coming soon theme.

5. Coming soon theme.

6. Coming soon theme.

== Changelog ==

= 1.4 =
* Fixed coming soon page logo uploader

= 1.2 =

* Added one more coming soon page theme.
* Fixed coming soon page LinkedIn Icon.



== Upgrade Notice ==

= 1.4 =
* Fixed coming soon page logo uploader

= 1.3 =

* Minor Changes

= 1.2 =

* Added one more coming soon page theme.
* Fixed coming soon page LinkedIn Icon.


