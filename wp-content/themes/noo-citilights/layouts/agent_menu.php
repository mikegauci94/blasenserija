<?php
global $current_user;
get_currentuserinfo();

$user_id			= $current_user->ID;
$agent_id			= get_user_meta($user_id, '_associated_agent_id',true );
$dash_profile		= noo_get_page_link_by_template( 'agent_dashboard_profile.php' );
$dash_properties	= noo_get_page_link_by_template( 'agent_dashboard.php' );
$dash_submit		= noo_get_page_link_by_template( 'agent_dashboard_submit.php' );

$page_template		= basename( get_page_template() );
$avatar_src			= wp_get_attachment_image_src( get_post_thumbnail_id( $agent_id ), 'full' );
if( empty($avatar_src) ) {
	$avatar_src		= NooAgent::get_default_avatar_uri();
} else {
	$avatar_src		= $avatar_src[0];
}

$membership_info	= NooAgent::get_membership_info( $agent_id );

$subscription_err   = false;
$err_message        = array();
$success            = false;

?>

<div class="user-sidebar-menu dashboard-sidebar">
	<div class="user-avatar content-thumb">
		<img src="<?php echo $avatar_src; ?>" alt="<?php echo $current_user->user_login; ?>"/>
	</div>
	<div class="user-menu-links">
		<?php if( !empty( $dash_profile ) ) : ?>
			<a href="<?php echo $dash_profile;?>" class="user-link <?php echo ( $page_template == 'agent_dashboard_profile.php' ? 'active' : '' ); ?>">
				<i class="fa fa-user"></i><?php _e('My Profile', 'noo');?>
			</a>
		<?php endif; ?>
		<?php if( !empty( $dash_properties ) ) : ?>
			<a href="<?php echo $dash_properties;?>" class="user-link <?php echo ( $page_template == 'agent_dashboard.php' ? 'active' : '' ); ?>">
				<i class="fa fa-home"></i><?php _e('My Properties', 'noo');?>
			</a>
		<?php endif; ?>
	</div>
	<div class="user-menu-links user-menu-logout">
		<a href="<?php echo wp_logout_url(NooAgent::get_login_url());?>" class="user-link" title="Logout">
			<i class="fa fa-sign-out"></i><?php _e('Log Out', 'noo');?>
		</a>
	</div>
	<?php if( $membership_info['type'] != 'none') : ?>
		<div class="user-menu-submit">
			<?php if( !empty( $dash_submit ) ) : ?>
				<a href="<?php echo $dash_submit;?>" class="btn btn-secondary <?php echo ( $page_template == 'agent_dashboard_submit.php' ? 'active' : '' ); ?>">
					<?php _e('SUBMIT PROPERTY', 'noo');?>
				</a>
			<?php endif; ?>
		</div>
	<?php endif; ?>
</div>




<?php if( !empty( $membership_info ) && $membership_info['type'] == 'membership' ) : ?>
	<h3 class="dashboard-sidebar-title"><?php _e('Your Current Plan', 'noo'); ?></h3>
	<div class="membership-info dashboard-sidebar">
		<div class="sidebar-content">
			<p><strong><?php echo $membership_info['data']['package_title']; ?></strong></p>
			<p><?php _e('Listing included', 'noo'); ?>:&nbsp;<?php echo $membership_info['data']['listing_included'] == -1 ? __('Unlimited', 'noo') : $membership_info['data']['listing_included']; ?></p>
			<p><?php _e('Listing remain', 'noo'); ?>:&nbsp;<?php echo $membership_info['data']['listing_remain'] == -1 ? __('Unlimited', 'noo') : $membership_info['data']['listing_remain']; ?></p>
			<!--		<p>--><?php //_e('Featured included', 'noo'); ?><!--:&nbsp;--><?php //echo $membership_info['data']['featured_included']; ?><!--</p>-->
			<!--		<p>--><?php //_e('Featured remain', 'noo'); ?><!--:&nbsp;--><?php //echo $membership_info['data']['featured_remain']; ?><!--</p>-->
			<!--		--><?php //$expired_date = $membership_info['data']['expired_date'];
			//		if( $expired_date === -1 ) :
			//		?>
			<!--		<p>--><?php //_e('Your package is expired', 'noo'); ?>
			<!--		--><?php //else: ?>
			<!--		<p>--><?php //_e('Ends On', 'noo'); ?><!--:&nbsp;--><?php //echo $expired_date; ?><!--</p>-->
			<!--		--><?php //endif; ?>
		</div>
	</div>

	<form id="password_form" name="password_form" class="noo-form profile-form" role="form">
		<div class="noo-control-group">
			<div class="group-title">
				<?php _e('Change Password', 'noo'); ?>
			</div>
			<div class="group-container row">
				<div class="form-message">
				</div>
				<div class="col-md-12">
					<div class="form-group s-profile-old_pass">
						<label for="old_pass"><?php _e('Old Password','noo'); ?></label>
						<input type="password" id="old_pass" class="form-control" value="" name="old_pass" />
					</div>
					<div class="form-group s-profile-new_pass">
						<label for="new_pass"><?php _e('New Password','noo'); ?></label>
						<input type="password" id="new_pass" class="form-control" value="" name="new_pass" />
					</div>
					<div class="form-group s-profile-new_pass_confirm">
						<label for="new_pass_confirm"><?php _e('Confirm New Password','noo'); ?></label>
						<input type="password" id="new_pass_confirm" class="form-control" value="" name="new_pass_confirm" />
					</div>
				</div>
				<div class="col-md-12">
					<div class="noo-submit">
						<input type="submit" class="btn btn-primary btn-lg" id="password_submit" value="<?php _e('Submit', 'noo'); ?>" />
					</div>
				</div>
			</div>
		</div>
		<input type="hidden" name="action" value="noo_ajax_change_password"/>
		<input type="hidden" name="user_id" value="<?php echo $user_id;?>">
		<?php wp_nonce_field('submit_profile_password','_noo_profile_password_nonce'); ?>
	</form>

	<!--<div class="membership-payment dashboard-sidebar">-->
	<!--	<div class="sidebar-content">-->
	<!--		--><?php //
//			$noo_payment_settings = get_option('noo_payment_settings', '');
//			$type_payment = NooPayment::get_payment_type();
//
//			if ( is_plugin_active( 'woocommerce/woocommerce.php' ) && $type_payment == 'woo' ) : ?>
	<!---->
	<!--				--><?php //$noo_ajax_payment = 'noo_ajax_payment_woo'; ?>
	<!---->
	<!--			--><?php //else : ?>
	<!--				-->
	<!--				--><?php //$noo_ajax_payment = 'noo_ajax_membership_payment';?>
	<!---->
	<!--			--><?php //endif; ?>
	<!---->
	<!--	</div>-->
	<!--</div>-->
<?php elseif( !empty( $membership_info ) && $membership_info['type'] == 'submission' ) : ?>
	<h3 class="dashboard-sidebar-title"><?php _e('Paid Submission', 'noo'); ?></h3>
	<div class="submission-info dashboard-sidebar">
		<div class="sidebar-content">
			<p><?php _e('This site uses paid submission model.', 'noo'); ?></p>
			<p><?php _e('Listing Price', 'noo'); ?>:&nbsp;<?php echo $membership_info['data']['listing_price_text']; ?></p>
			<p style="display: none"><?php _e('Featured Price (extra)', 'noo'); ?>:&nbsp;<?php echo $membership_info['data']['featured_price_text']; ?></p>
		</div>
	</div>
<?php endif; ?>

 